import {
  Directive,
  HostListener,
  ElementRef,
  Renderer2,
  HostBinding
} from "@angular/core";

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  //constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  @HostBinding('class.open') open: boolean = false;

  @HostListener('click') toggleOpen(){
    this.open = !this.open;
    // if (!this.open) {
    //   this.renderer.addClass(this.elementRef.nativeElement, 'open');
    //   this.open = true;
    // } else {
    //   this.renderer.removeClass(this.elementRef.nativeElement, 'open')
    //   this.open = false;
    // }
  }

}
