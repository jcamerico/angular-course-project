"use strict";
/**
 * Created by jamerico on 20/07/2017.
 */
var Ingredient = (function () {
    function Ingredient(name, amount) {
        this.name = name;
        this.amount = amount;
    }
    return Ingredient;
}());
exports.Ingredient = Ingredient;
