/**
 * Created by jamerico on 20/07/2017.
 */
export class Ingredient {

  constructor(public name: string, public amount: number) {
  }

}
