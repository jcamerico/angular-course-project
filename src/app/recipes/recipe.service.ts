import {
  Injectable,
  EventEmitter
} from '@angular/core';

import { Recipe } from "./recipe.model";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";

@Injectable()
export class RecipeService {

  recipeSelected = new EventEmitter<Recipe>();

  private recipes : Recipe[] = [
    new Recipe('A Test Recipe', 'This is a test', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Banjo_Shark_recipe.jpg/320px-Banjo_Shark_recipe.jpg', [new Ingredient('Meat', 1), new Ingredient('Fries', 20)]),
    new Recipe('Another Test Recipe', 'This is another test', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Banjo_Shark_recipe.jpg/320px-Banjo_Shark_recipe.jpg', [new Ingredient('Bread', 1), new Ingredient('Meat', 1)])
  ];

  constructor(private shoppingListService: ShoppingListService) { }

  getRecipes(){
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(recipe: Recipe){
    this.shoppingListService.addIngredient(...recipe.ingredients);
  }

  getRecipe(index: number){
    return this.recipes[index];
  }


}
