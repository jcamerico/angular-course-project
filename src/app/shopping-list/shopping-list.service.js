"use strict";
var ingredient_model_1 = require("../shared/ingredient.model");
var core_1 = require("@angular/core");
var ShoppingListService = (function () {
    function ShoppingListService() {
        this.ingredientsChanged = new core_1.EventEmitter();
        this.ingredients = [
            new ingredient_model_1.Ingredient('Apples', 5),
            new ingredient_model_1.Ingredient('Tomatoes', 10)
        ];
    }
    ShoppingListService.prototype.getIngredients = function () {
        return this.ingredients.slice();
    };
    ShoppingListService.prototype.addIngredient = function () {
        var ingredient = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            ingredient[_i - 0] = arguments[_i];
        }
        (_a = this.ingredients).push.apply(_a, ingredient);
        this.ingredientsChanged.emit(this.ingredients);
        var _a;
    };
    return ShoppingListService;
}());
exports.ShoppingListService = ShoppingListService;
