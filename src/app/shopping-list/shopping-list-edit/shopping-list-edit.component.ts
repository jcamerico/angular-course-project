import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { Ingredient } from "../../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list.service";

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit {

  @ViewChild('nameInput') nameInputChild: ElementRef;
  @ViewChild('amountInput') amountInputChild: ElementRef;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
  }

  onAdd(){
    this.shoppingListService.addIngredient(
      new Ingredient(
        this.nameInputChild.nativeElement.value,
        this.amountInputChild.nativeElement.value)
    );
  }

  onClear(){
    this.nameInputChild.nativeElement.value = '';
    this.amountInputChild.nativeElement.value = '';
  }

}
