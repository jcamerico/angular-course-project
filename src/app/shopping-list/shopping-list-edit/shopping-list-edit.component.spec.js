"use strict";
var testing_1 = require('@angular/core/testing');
var shopping_list_edit_component_1 = require('./shopping-list-edit.component');
describe('ShoppingListEditComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [shopping_list_edit_component_1.ShoppingListEditComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(shopping_list_edit_component_1.ShoppingListEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should be created', function () {
        expect(component).toBeTruthy();
    });
});
