"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var ingredient_model_1 = require("../../shared/ingredient.model");
var ShoppingListEditComponent = (function () {
    function ShoppingListEditComponent(shoppingListService) {
        this.shoppingListService = shoppingListService;
    }
    ShoppingListEditComponent.prototype.ngOnInit = function () {
    };
    ShoppingListEditComponent.prototype.onAdd = function () {
        this.shoppingListService.addIngredient(new ingredient_model_1.Ingredient(this.nameInputChild.nativeElement.value, this.amountInputChild.nativeElement.value));
    };
    ShoppingListEditComponent.prototype.onClear = function () {
        this.nameInputChild.nativeElement.value = '';
        this.amountInputChild.nativeElement.value = '';
    };
    __decorate([
        core_1.ViewChild('nameInput')
    ], ShoppingListEditComponent.prototype, "nameInputChild", void 0);
    __decorate([
        core_1.ViewChild('amountInput')
    ], ShoppingListEditComponent.prototype, "amountInputChild", void 0);
    ShoppingListEditComponent = __decorate([
        core_1.Component({
            selector: 'app-shopping-list-edit',
            templateUrl: './shopping-list-edit.component.html',
            styleUrls: ['./shopping-list-edit.component.css']
        })
    ], ShoppingListEditComponent);
    return ShoppingListEditComponent;
}());
exports.ShoppingListEditComponent = ShoppingListEditComponent;
